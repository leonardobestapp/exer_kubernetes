nextflow.preview.dsl=2


workflow {
  test()
}

process test {
  publishDir "publish_dir_wei", mode: "copy"

  echo true

  output: 
  file('generated_wei.file')

  script:
  """
  
  echo '------- hello from test'
  ls -R /mnt/data
  
  echo '------- hello from test' >generated_wei.file
 
  """
}
